package jackmego.com.jieba_android;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import androidx.collection.SimpleArrayMap;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okio.BufferedSource;
import okio.Okio;
import okio.Source;

import static jackmego.com.jieba_android.Utility.LOGTAG;

public class WordDictionary {
    private static WordDictionary singleton;

    private static final String MAIN_DICT = "jieba/dict_out.txt";
    private static final String MAIN_PROCESSED = "dict_processed.txt";
    private static final String OUTFILE = "jieba/" + MAIN_PROCESSED;

    public final SimpleArrayMap<String, String> freqs = new SimpleArrayMap<>(); // 加载中间文件的话，这里也要改
    public Element restoredElement; // 生成中间文件的时候要修改这个变量的引用为element
    private Double minFreq = Double.MAX_VALUE;
    private Double total = 0.0;
    public Element element;
    private StringBuilder dicLineBuild = new StringBuilder();
    private final String TAB = "\t";
    private final String SHARP = "#";
    private final String SLASH = "/";
    private final String DOLLAR = "$";

    private WordDictionary(AssetManager assetManager) {


        // 实际运行的时候直接使用下面的代码加载该中间文件
        //long start = System.currentTimeMillis();

        // 加载字典文件
        List<String> strArray = getStrArrayFromFile(assetManager);

        if (strArray == null) {
            Log.d(LOGTAG, "getStrArrayFromFile failed, stop");
            return;
        }

        restoredElement = new Element((char) 0);
        ArrayList<Element> elemArr = new ArrayList<>();
        elemArr.add(restoredElement);

        restoreElement(elemArr, strArray, 0);

        //long end = System.currentTimeMillis();
        //Log.d(LOGTAG, String.format("restoreElement takes %d ms", end - start));
    }

    /**
     * 预处理，生成中间文件
     *
     * @param assetManager
     */
    public void preProcess(Context context, AssetManager assetManager) {
        boolean result = this.loadDict(assetManager);

        if (result) {
            ArrayList<Element> arr = new ArrayList<>();
            arr.add(element);
            saveDictToFile(context, arr);
        } else {
            Log.e(LOGTAG, "Error");
        }



        /* 这段代码用于测试从sd卡读取
        List<String> strArray = null;
        try {
            File file = new File(Environment.getExternalStorageDirectory(), MAIN_PROCESSED);
            BufferedReader br = new BufferedReader(new FileReader(file));
            String readline = br.readLine();

            strArray = java.util.Arrays.asList(readline.split(TAB));
            br.close();
            Log.d(LOGTAG, "读取成功：" + readline);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    private ArrayList<Element> newElemArray = new ArrayList<>();

    /**
     * d/b/c/	g/	f/e/	#/	j/	#/	h/	#/	#/
     */
    private void restoreElement(ArrayList<Element> elemArray, List<String> strArray, int startIndex) {
        if (elemArray.size() <= 0) {
            return;
        }

        newElemArray.clear();

        for (int i = 0; i < elemArray.size(); i++) {
            String strCluster = strArray.get(startIndex);
            String[] strList = strCluster.split(SLASH);

            Element e = elemArray.get(i);
            // #/
            if (strList.length == 1 && strList[0].equalsIgnoreCase(SHARP)) {
                e.nodeState = 1;
                e.storeSize = 0;
            } else { //  f/e/
                e.childrenMap = new SimpleArrayMap<>();
                for (String s : strList) {
                    boolean isWord = s.length() == 2;
                    Character ch = s.charAt(0);
                    Element childElem = new Element(ch);
                    childElem.nodeState = isWord ? 1 : 0;

                    e.childrenMap.put(ch, childElem);
                    e.storeSize++;

                    newElemArray.add(childElem);
                }
            }

            startIndex++;
        }

        restoreElement(newElemArray, strArray, startIndex);
    }


    /**
     * ROOT
     * b/  -- c$/   --  d/
     * e$/f/ -- #/   --  g/
     * h$/ ---- #/  ---- i$/
     * #/  --------- #/
     *
     * @param elementArray
     */
    private void saveDictToFile(Context context, ArrayList<Element> elementArray) {
        if (elementArray.size() <= 0) {
            Log.d(LOGTAG, "saveDictToFile final str: " + dicLineBuild.toString());

            try {

                FileOutputStream fos = context.openFileOutput(MAIN_PROCESSED, Context.MODE_PRIVATE);

                // 第一行是字典数据
                dicLineBuild.append("\r\n");

                // 第二行： 最小频率 TAB 单词1 TAB 频率 TAB 单词2 TAB 频率 ...
                dicLineBuild.append(minFreq);


                for (int i = 0; i < freqs.size(); i++) {
                    dicLineBuild.append(TAB);
                    dicLineBuild.append(freqs.keyAt(i));
                    dicLineBuild.append(TAB);
                    dicLineBuild.append(freqs.valueAt(i));
                }

                fos.write(dicLineBuild.toString().getBytes());

                fos.close();

                Log.d(LOGTAG, String.format("字典中间文件生成成功，存储在%s", "1111111"));
            } catch (Exception e) {
                Log.d(LOGTAG, "字典中间文件生成失败！");
                e.printStackTrace();
            }

            return;
        }

        ArrayList<Element> childArray = new ArrayList<>();
        // elementArray有几个元素，就要添加TAB分割的几个数据段，每个数据段是该Element的子节点的字+"/"，比如 e/f/ TAB #/ TAB g/
        // 如果从根节点到当前节点的路径表示一个词，那么在后面添加$符号,如  e$/f/ TAB #/ TAB g/
        for (int i = 0; i < elementArray.size(); i++) {
            Element element = elementArray.get(i);

            // e/f/
            if (element.hasNextNode()) {

                for (int index = 0; index < element.childrenMap.size(); index++) {
                    Character key = element.childrenMap.keyAt(index);
                    dicLineBuild.append(key);

                    Element value = element.childrenMap.valueAt(index);

                    if (value.nodeState == 1) {
                        dicLineBuild.append(DOLLAR);  // 从根节点到当前节点的路径表示一个词，那么在后面添加$符号,如  e$/f/ TAB #/ TAB g/
                    }

                    dicLineBuild.append(SLASH);

                    // 将该节点的所有子节点入列表，供下一次递归
                    childArray.add(value);
                }
            } else { // #/
                dicLineBuild.append(SHARP);
                dicLineBuild.append(SLASH);
            }

            // TAB
            dicLineBuild.append(TAB);
        }

        saveDictToFile(context, childArray);
    }


    public static WordDictionary getInstance(AssetManager assetManager) {
        if (singleton == null) {
            synchronized (WordDictionary.class) {
                if (singleton == null) {
                    singleton = new WordDictionary(assetManager);
                    return singleton;
                }
            }
        }
        return singleton;
    }

    public boolean loadDict(AssetManager assetManager) {
        element = new Element((char) 0); // 创建一个根Element，只有一个，其他的Element全是其子孙节点
        InputStream is = null;
        try {

            is = assetManager.open(MAIN_DICT);

            if (is == null) {
                Log.e(LOGTAG, "Load asset file error:" + MAIN_DICT);
                return false;
            }

            Source fileSource = Okio.source(is);
            BufferedSource bufferedSource = Okio.buffer(fileSource);

            while (!bufferedSource.exhausted()) {
                String line = bufferedSource.readUtf8Line();
                if (line == null) break;

                String[] tokens = line.split("[\t ]+");
                if (tokens.length < 2)
                    continue;

                String word = tokens[0]; // eg:一两千块
                double freq = Double.parseDouble(tokens[1]);
                total += freq;
                String trimmedword = addWord(word);  // 将一个单词的每个字递归的插入字典树  eg:一两千块
                //freqs.put(trimmedword, freq);        // 并统计单词首个字的频率
            }

            bufferedSource.close();
            fileSource.close();

            // normalize
            /*for (int i = 0; i < freqs.size(); i++) {
                freqs.setValueAt(i, freqs.valueAt(i) / total);
                minFreq = Math.min(freqs.valueAt(i), minFreq);
            }*/

            //Log.d(LOGTAG, String.format("main dict load finished, time elapsed %d ms", System.currentTimeMillis() - s));
        } catch (IOException e) {
            //Log.e(LOGTAG, String.format("%s load failure!", MAIN_DICT));
            return false;
        } finally {
            try {
                if (null != is)
                    is.close();
            } catch (IOException e) {
                //Log.e(LOGTAG, String.format("%s close failure!", MAIN_DICT));
                return false;
            }
        }

        return true;
    }


    /**
     * 将一个单词的每个字递归的插入字典树
     *
     * @param word
     * @return
     */
    private String addWord(String word) {
        if (null != word && !"".equals(word.trim())) {
            String key = word.trim().toLowerCase(Locale.getDefault());
            element.fillElement(key.toCharArray());
            return key;
        } else
            return null;
    }

    public Element getTrie() {
        return this.restoredElement; //this.element;
    }


    public boolean containsWord(String word) {
        return freqs.containsKey(word);
    }


    public Double getFreq(String key) {
        if (containsWord(key))
            return Double.valueOf(freqs.get(key));
        else
            return minFreq;
    }

    public List<String> getStrArrayFromFile(AssetManager assetManager) {
        List<String> strArray;

        InputStream is = null;
        try {
            is = assetManager.open(OUTFILE);

            if (is == null) {
                Log.e(LOGTAG, "Load asset file error:" + OUTFILE);
                return null;
            }

            Source fileSource = Okio.source(is);
            BufferedSource bufferedSource = Okio.buffer(fileSource);

            // 第一行是字典文件
            String dictLine = bufferedSource.readUtf8Line();
            strArray = java.util.Arrays.asList(dictLine.split(TAB));

            // 第二行是：最小频率 TAB 单词1 TAB 频率 TAB 单词2 TAB 频率 ...
            String freqLine = bufferedSource.readUtf8Line();
            //final List<String> strArray2 = java.util.Arrays.asList(freqLine.split(TAB));
            final String[] split = freqLine.split(TAB);
            minFreq = Double.valueOf(split[0]);

            final int wordCnt = (split.length - 1) / 2;

            // freqs.put操作需要3秒才能完成，所以放在一个线程中异步进行，在map加载完成之前调用分词会不那么准确，但是不会报错
            new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < wordCnt; i++) {
                        freqs.put(split[2 * i + 1], split[2 * i + 2]);
                    }
                }
            }).start();

            is.close();

            bufferedSource.close();
            fileSource.close();


            return strArray;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
